﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace InPlaceSample.Models
{
    /// <summary>
    /// Theme Read Model
    /// </summary>
    public class ThemeModel : ThemeUpdateModel
    {
        /// <summary>
        /// Theme Id
        /// </summary>
        [Required]
        public int ThemeId { get; set; }
    }

    /// <summary>
    /// Theme Update Model
    /// </summary>
    public class ThemeUpdateModel
    {
        /// <summary>
        /// Theme Name
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Theme Description
        /// </summary>
        [Required]
        public string Description { get; set; }
    }

    internal class Result<T>
    {
        public Result()
        {
            Messages = new List<SystemMessage>();
        }
        public List<SystemMessage> Messages { get; set; }
        public T Value { get; set; }

        public bool IsSuccess
        {
            get { return Messages.Count == 0; }
        }
    }
}