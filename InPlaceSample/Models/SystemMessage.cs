namespace InPlaceSample.Models
{
    /// <summary>
    /// Validation Message
    /// </summary>
    public class SystemMessage
    {
        /// <summary>
        /// ctor
        /// </summary>
        public SystemMessage()
        {
            
        }

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="message">Validation Message</param>
        public SystemMessage(string message)
        {
            Message = message;
        }

        /// <summary>
        /// The Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Message;
        }
    }
}