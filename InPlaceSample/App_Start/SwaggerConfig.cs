using System.Web.Http;
using WebActivatorEx;
using InPlaceSample;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace InPlaceSample
{
    internal class SwaggerConfig
    {
        internal static void Register()
        {
            var containingAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration 
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "InPlaceSample");
                        c.IncludeXmlComments(string.Format(@"{0}\bin\InPlaceSample.XML", System.AppDomain.CurrentDomain.BaseDirectory));
                    })
                .EnableSwaggerUi(c =>
                    {
                        c.InjectStylesheet(containingAssembly, "InPlaceSample.Content.CustomAssets.screen.css");
                        c.CustomAsset("index", containingAssembly, "InPlaceSample.Content.CustomAssets.index.html");
                    });

        }
    }
}
