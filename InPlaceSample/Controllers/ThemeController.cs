﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using InPlaceSample.Models;

namespace InPlaceSample.Controllers
{
    /// <summary>
    /// Themes Actions
    /// </summary>
    [RoutePrefix("api/Themes")]
    public class ThemesController : ApiController
    {
        static readonly List<ThemeModel> Themes = new List<ThemeModel>
        {
            new ThemeModel
            {
                ThemeId = 1,
                Name = "Theme 1",
                Description = "The First Theme"
            },
             new ThemeModel
            {
                ThemeId = 2,
                Name = "Theme 2",
                Description = "The Second Theme"
            }
        };

        /// <summary>
        /// Lists All Themes
        /// </summary>
        /// <returns></returns>
        /// <response code="200"></response>
        [ResponseType(typeof(List<ThemeModel>))]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, Themes);
        }

        /// <summary>
        /// Gets a Theme
        /// </summary>
        /// <param name="id">The Theme Id</param>
        /// <returns></returns>
        /// <response code="200"></response>
        /// <response code="404">Theme Not Found</response>
        [ResponseType(typeof(ThemeModel))]
        public HttpResponseMessage Get(int id)
        {
            var theme = Themes.FirstOrDefault(x => x.ThemeId == id);
            if (theme == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return Request.CreateResponse(HttpStatusCode.OK, theme);
        }

        /// <summary>
        /// Saves a Theme
        /// </summary>
        /// <param name="data">The Theme to be Saved</param>
        /// <returns></returns>
        /// <response code="201">Theme Saved</response>
        /// <response code="400">Validation Failed</response>
        [ResponseType(typeof(ThemeModel))]
        public HttpResponseMessage Post([FromBody]ThemeUpdateModel data)
        {
            var saveResult = CreateTheme(data); //returns Type Result<SaveThemeModel>
            if (!saveResult.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, saveResult.Messages); //Status 400
            }
            // the newly created Same Theme is Created, this could also be mapped to the Read version of this model
            return Request.CreateResponse(HttpStatusCode.Created, saveResult.Value);  //Status 201
        }

        /// <summary>
        /// Saves a Theme
        /// </summary>
        /// <param name="id">The Theme id of the theme to be updated</param>
        /// <param name="data">The Theme Data to be Saved</param>
        /// <returns></returns>
        /// <response code="200">Theme Updated</response>
        /// <response code="400">Validation Failed</response>
        /// <response code="404">Theme Not Found</response>
        [ResponseType(typeof(ThemeModel))]
        public HttpResponseMessage Put(int id, [FromBody]ThemeUpdateModel data)
        {
            var result = UpdateTheme(id, data); //returns Type Result<SaveThemeModel>
            if (!result.IsSuccess)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, result.Messages); //Status 400
            }

            if (result.Value == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound); //Status 404
            }
            // the updated Theme is returned with the 200 status
            return Request.CreateResponse(HttpStatusCode.OK, result.Value);  //Status 200
        }

        /// <summary>
        /// Delete a Theme
        /// </summary>
        /// <param name="id">The ID if the Theme to delete</param>
        /// <returns></returns>
        /// <response code="204">Delete Succsessful</response>
        /// <response code="400">Validation Failed</response>
        /// <response code="404">Theme not Found</response>
        [ResponseType(typeof(void))]
        public HttpResponseMessage Delete(int id)
        {
            var theme = Themes.FirstOrDefault(x => x.ThemeId == id);
            if (theme == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            if (id == 1 || id == 2)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return Request.CreateResponse(HttpStatusCode.NoContent);
        }

        private Result<ThemeModel> CreateTheme(ThemeUpdateModel themeUpdateModel)
        {
            var result = new Result<ThemeModel>();

            if (string.IsNullOrEmpty(themeUpdateModel.Name))
            {
                result.Messages.Add(new SystemMessage("Name Required"));
            }

            if (string.IsNullOrEmpty(themeUpdateModel.Description))
            {
                result.Messages.Add(new SystemMessage("Description Required"));
            }

            var theme = new ThemeModel
            {
                ThemeId = Themes.Select(x => x.ThemeId).Max() + 1,
                Name = themeUpdateModel.Name,
                Description = themeUpdateModel.Description
            };

            Themes.Add(theme);

            result.Value = theme;

            return result;
        }

        private Result<ThemeModel> UpdateTheme(int id, ThemeUpdateModel themeUpdateModel)
        {
            var result = new Result<ThemeModel>();

            if (string.IsNullOrEmpty(themeUpdateModel.Name))
            {
                result.Messages.Add(new SystemMessage("Name Required"));
            }

            if (string.IsNullOrEmpty(themeUpdateModel.Description))
            {
                result.Messages.Add(new SystemMessage("Description Required"));
            }

            var theme = Themes.FirstOrDefault(x => x.ThemeId == id);
            if (theme != null)
            {
                theme.Name = themeUpdateModel.Name;
                theme.Description = themeUpdateModel.Description;
                result.Value = theme;
            }

            return result;
        }

    }
}
