﻿declare module app {
    
    module data {
        interface IThemeModel {
            themeId: number;
            name: string;
            description: string;
        }

        interface IUpdateThemeModel {
            name: string;
            description: string;
        }

        interface INetworkResponse {
            data: any,
            status: number;
        }
    }
}

