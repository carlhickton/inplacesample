﻿module app.data.Factories {
    "use strict";

    export class ThemeFactory {

        url : string;

        constructor(private $q: ng.IQService, private $http: ng.IHttpService) {
            this.url = ""; //"https://inplace-qa1:8085/";
        }

        getThemes(): ng.IPromise<Array<IThemeModel>> {
            var deferred = this.$q.defer();
            this.$http.get(this.url + "api/Themes")
                .success((response: Array<IThemeModel>) => {

                    deferred.resolve(response);

                }).error((data, status) => {
                    deferred.reject(<INetworkResponse>{ data, status });
                });
            return deferred.promise;
        }

        getTheme(id: number): ng.IPromise<IThemeModel> {
            var deferred = this.$q.defer();
            this.$http.get(this.url + "api/Themes/" + id)
                .success((response: IThemeModel) => {
                    deferred.resolve(response);
                }).error((data, status) => {
                    deferred.reject(<INetworkResponse>{ data, status });
                });
            return deferred.promise;
        }
    }

    function factory($q: ng.IQService, $http: ng.IHttpService): ThemeFactory {
        return new ThemeFactory($q, $http);
    }
    factory.$inject = ["$q", "$http"];
    angular.module("app.data").factory("themeFactory", factory);
}