var app;
(function (app) {
    var data;
    (function (data_1) {
        var Factories;
        (function (Factories) {
            "use strict";
            var ThemeFactory = (function () {
                function ThemeFactory($q, $http) {
                    this.$q = $q;
                    this.$http = $http;
                    this.url = ""; //"https://inplace-qa1:8085/";
                }
                ThemeFactory.prototype.getThemes = function () {
                    var deferred = this.$q.defer();
                    this.$http.get(this.url + "api/Themes")
                        .success(function (response) {
                        deferred.resolve(response);
                    }).error(function (data, status) {
                        deferred.reject({ data: data, status: status });
                    });
                    return deferred.promise;
                };
                ThemeFactory.prototype.getTheme = function (id) {
                    var deferred = this.$q.defer();
                    this.$http.get(this.url + "api/Themes/" + id)
                        .success(function (response) {
                        deferred.resolve(response);
                    }).error(function (data, status) {
                        deferred.reject({ data: data, status: status });
                    });
                    return deferred.promise;
                };
                return ThemeFactory;
            })();
            Factories.ThemeFactory = ThemeFactory;
            function factory($q, $http) {
                return new ThemeFactory($q, $http);
            }
            factory.$inject = ["$q", "$http"];
            angular.module("app.data").factory("themeFactory", factory);
        })(Factories = data_1.Factories || (data_1.Factories = {}));
    })(data = app.data || (app.data = {}));
})(app || (app = {}));
//# sourceMappingURL=ThemeFactory.js.map