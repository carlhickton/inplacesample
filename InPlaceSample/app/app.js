(function () {
    "use strict";
    angular
        .module("app", [
        "ui.router",
        "app.data"
    ]);
    function config($compileProvider, $stateProvider, $urlRouterProvider) {
        $compileProvider.debugInfoEnabled(false);
        $stateProvider.state("home", {
            url: "/",
            templateUrl: "app/sample/views/home.html",
            controller: "homeController",
            controllerAs: "vm"
        });
        $urlRouterProvider.otherwise("/");
    }
    ;
    angular
        .module("app")
        .config(config);
    config.$inject = ["$compileProvider", "$stateProvider", "$urlRouterProvider"];
})();
//# sourceMappingURL=app.js.map