var app;
(function (app) {
    var sample;
    (function (sample) {
        "use strict";
        var HomeController = (function () {
            function HomeController($scope, themeFactory, $log) {
                var _this = this;
                this.$scope = $scope;
                this.themeFactory = themeFactory;
                this.$log = $log;
                this.title = "Themes Page";
                this.items = new Array();
                themeFactory.getThemes().then(function (data) {
                    _this.items = data;
                }).catch(function (err) {
                    _this.$log.error(err);
                });
                themeFactory.getTheme(20).then(function (data) {
                    _this.$log.debug(data);
                }).catch(function (err) {
                    _this.$log.error(err);
                    if (err.status === 404) {
                        _this.message = "Theme 50 not Found";
                    }
                });
            }
            HomeController.$inject = ["$scope", "themeFactory", "$log"];
            return HomeController;
        })();
        angular.module("app").controller("homeController", HomeController);
    })(sample = app.sample || (app.sample = {}));
})(app || (app = {}));
//# sourceMappingURL=HomeController.js.map