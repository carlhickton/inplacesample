﻿module app.sample {
    "use strict";


    class HomeController {
        static $inject = ["$scope", "themeFactory", "$log"];

        items: Array<app.data.IThemeModel>;
        title: string;
        message: string;

        constructor(private $scope: ng.IScope, private themeFactory: app.data.Factories.ThemeFactory, private $log : ng.ILogService) {
            this.title = "Themes Page";
            this.items = new Array();

            themeFactory.getThemes().then(data => {
                this.items = data;
            }).catch((err: app.data.INetworkResponse) => {
                this.$log.error(err);
            });

            themeFactory.getTheme(20).then(data => {
                this.$log.debug(data);
            }).catch((err: app.data.INetworkResponse) => {
                this.$log.error(err);
                if (err.status === 404) {
                    this.message = "Theme 50 not Found";
                }
            });

        }

    }

    angular.module("app").controller("homeController", HomeController);
}