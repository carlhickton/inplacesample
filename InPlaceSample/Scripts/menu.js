﻿(function () {

    function activate() {

        var item = $(this);

        item.find(".menu_subnav").slideDown("fast").show();
        item.addClass("menuHover");

        item.hover(
            function() {
            },
            function() {
                item.find(".menu_subnav").slideUp("fast");
                item.removeClass("menuHover");
            });
    }

    function activateFocusin() {

        var item = $(this);

        // If the focus is on top menu item no other top menu item should have focus
        if (item.hasClass("menu_icon") || item.hasClass("menu_section")) {
            // I am a top item now navigate all my siblings and shut them up if they are open
            // Except me ofcourse!!
            $(".menu_navbar li").each(function() {
                if ($(this).find("a").attr("id") != item.find("a").attr("id")) {
                    $(this).find(".menu_subnav").slideUp("fast");
                    $(this).removeClass("menuHover");
                }
            });
        }

        item.addClass("menuHover");
    }

    function activateFocusout() {

        var item = $(this);

        if (item.find("menuHover") && item.find(".menu_subnav").is(":visible")) {
            return;
        }

        if (item.find("menuHover")) {
            item.find(".menu_subnav").slideUp("fast");
        }
        item.removeClass("menuHover");

    }

    $(".menu_navbar li").click(activate);
    $(".menu_navbar li").focusin(activateFocusin);
    $(".menu_navbar li").focusout(activateFocusout);

})()